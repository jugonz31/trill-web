$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
    interval: 2000   
  });

  $('#contacto').on('show.bs.modal', function (e){
    console.log("Mostrando modal")
    $('#contactoBtn').removeClass('btn-success')
    $('#contactoBtn').addClass('btn-default')
    $('#contactoBtn').prop('disabled',true)
  })
  $('#contacto').on('shown.bs.modal', function (e){
    console.log("Se mostro modal")
  })
  $('#contacto').on('hide.bs.modal', function (e){
    console.log("Modal oculto")
  })
  $('#contacto').on('hidden.bs.modal', function (e){
    console.log("Se oculto modal")
  })
  });